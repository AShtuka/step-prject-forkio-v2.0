const pricingBtn = document.querySelector(".pricing-item-container");
pricingBtn.addEventListener("touchstart", function (event) {
    event.preventDefault();
    if (event.target.className === "pricing-item_btn") {
        if (document.querySelector(".pricing-item_btn--active")) {
            document.querySelector(".pricing-item_btn--active").classList.toggle("pricing-item_btn--active");
        }
        event.target.classList.toggle("pricing-item_btn--active");
    }
},false);